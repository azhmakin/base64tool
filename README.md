# README #

## Base64 Tool ##

A GUI application to process base64 encoded data.

The application allows you to do the following:

* Encode and decode base64 strings
* Extract base64 encoded resources from XML (including SVG) files
