//
// Created by Andrey Zhmakin, 2017.
//

package zhmakin.base64;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Andrey Zhmakin
 */
public class Base64EncodingStreamTest
{
    final static String MAN_PLAIN =
            "Man is distinguished, not only by his reason, but by this singular passion from " +
            "other animals, which is a lust of the mind, that by a perseverance of delight " +
            "in the continued and indefatigable generation of knowledge, exceeds the short " +
            "vehemence of any carnal pleasure.";


    @Test
    public void test_01()
    {
        Assert.assertEquals("", Base64EncodingStream.encode(MAN_PLAIN), (Base64DecodingStreamTest.MAN_BASE64));
    }
}
