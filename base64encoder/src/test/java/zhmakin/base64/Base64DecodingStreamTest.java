//
// Created by Andrey Zhmakin, 2017.
//

package zhmakin.base64;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Andrey Zhmakin
 */
public class Base64DecodingStreamTest
{
    final static String MAN_BASE64 =
            "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlz\n" +
            "IHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg\n" +
            "dGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGlu\n" +
            "dWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRo\n" +
            "ZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";


    @Test
    public void test_01()
    {
        Assert.assertEquals("", Base64DecodingStream.decode(MAN_BASE64), (Base64EncodingStreamTest.MAN_PLAIN));
    }
}
