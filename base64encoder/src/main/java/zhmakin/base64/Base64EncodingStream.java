//
// Created by Andrey Zhmakin, 2017.
//

package zhmakin.base64;

import java.io.*;


/**
 * @author Andrey Zhmakin
 */
public class Base64EncodingStream
    extends OutputStream
{
    private OutputStream stream;
    private int buffer;
    private int occupied;

    private int count;
    private int lineLength;


    public Base64EncodingStream(OutputStream stream)
    {
        this.stream = stream;
    }


    public Base64EncodingStream(OutputStream stream, int lineLength)
    {
        this.stream = stream;
        this.lineLength = lineLength;
    }


    @Override
    public void write(int b)
        throws IOException
    {
        switch (this.occupied)
        {
            case 0:
                this.writeChar(toChar( (b >> 2) & 0b111111 ));
                this.buffer = (b << 4) & 0b110000;
                this.occupied = 2;
                break;

            case 2:
                this.buffer |= 0b001111 & (b >> 4);
                this.writeChar(toChar(this.buffer));
                this.buffer = 0b111100 & (b << 2);
                this.occupied = 4;
                break;

            case 4:
                this.buffer |= 0b000011 & (b >> 6);
                this.writeChar(toChar(this.buffer));
                this.writeChar(toChar(b & 0b111111));
                this.occupied = 0;
                break;

            default:
                assert false : "Illegal branch!";
        }
    }


    @Override
    public void flush()
            throws IOException
    {
        this.stream.flush();
    }


    @Override
    public void close()
        throws IOException
    {
        switch (this.occupied)
        {
            case 2:
                this.writeChar(toChar(this.buffer));
                this.writeChar('=');
                this.writeChar('=');
                break;

            case 4:
                this.writeChar(toChar(this.buffer));
                this.writeChar('=');
                break;
        }

        this.stream.flush();
    }


    public static int toChar(int value)
    {
        assert value >= 0 && value <= 63 : "value = " + value;

        if (value <= 25)
        {
            return 'A' + value;
        }
        else if (value <= 51)
        {
            return 'a' + value - 26;
        }
        else if (value <= 61)
        {
            return '0' + value - 52;
        }
        else if (value == 62)
        {
            return '+';
        }
        else
        {
            return '/';
        }
    }


    private void writeChar(int c)
        throws IOException
    {
        this.stream.write(c);

        if (this.lineLength > 0)
        {
            this.count++;

            if (this.count == this.lineLength)
            {
                this.count = 0;
                this.stream.write('\n');
            }
        }
    }


    public static String encode(String input)
    {
        try
        {
            InputStream is = new ByteArrayInputStream( input.getBytes( "UTF-8" ) );
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            Base64EncodingStream encodingStream = new Base64EncodingStream(os, 76);

            int c;

            while ((c = is.read()) != -1)
            {
                encodingStream.write(c);
            }

            encodingStream.close();
            os.close();

            return os.toString();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
