//
// Created by Andrey Zhmakin, 2017.
//

package zhmakin.base64;

import java.io.*;


/**
 * @author Andrey Zhmakin
 */
public class Base64DecodingStream
    extends OutputStream
{
    private final static byte[] VALUE = new byte[256];

    static
    {
        for (int i = 0; i < 256; i++)
        {
            VALUE[i] = -2;
        }

        for (int i = 0; i < 64; i++)
        {
            VALUE[Base64EncodingStream.toChar(i)] = (byte) i;
        }

        VALUE['='] = -1;

        VALUE[' '] = -1;
        VALUE['\t'] = -1;
        VALUE['\n'] = -1;
        VALUE['\r'] = -1;
    }

    private OutputStream stream;

    private int buffer;
    private int occupied;

    public Base64DecodingStream(OutputStream stream)
    {
        this.stream = stream;
    }


    @Override
    public void write(int c)
            throws IOException
    {
        int b = VALUE[c & 255];

        switch (b)
        {
            case -2:
                throw new IOException("Illegal character in input!");

            case -1:
                // Ignore!
                break;

            default:
                this.processCharacter(b);
        }
    }


    private void processCharacter(int c)
        throws IOException
    {
        assert c >= 0 && c <= 63;

        switch (this.occupied)
        {
            case 0:
                this.buffer = c << 2;
                this.occupied = 6;
                break;

            case 6:
                this.buffer |= (c >> 4)/* & 0b000011*/;
                this.writeChar(this.buffer);
                this.buffer = (c << 4) & 0b11110000;
                this.occupied = 4;
                break;

            case 4:
                this.buffer |= (c >> 2)/* & 0b001111*/;
                this.writeChar(this.buffer);
                this.buffer = (c & 0b11) << 6;
                this.occupied = 2;
                break;

            case 2:
                this.buffer |= c;
                this.writeChar(this.buffer);
                this.occupied = 0;
                break;

            default:
                assert false : "Illegal branch!";
        }
    }


    private void writeChar(int c)
        throws IOException
    {
        this.stream.write(c);
    }


    public static String decode(String input)
    {
        byte[] bytes = decodeBytes(input);

        return bytes != null ? new String(bytes, 0, bytes.length) : null;
    }


    public static byte[] decodeBytes(String input)
    {
        try
        {
            InputStream is = new ByteArrayInputStream( input.getBytes( "UTF-8" ) );
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            Base64DecodingStream encodingStream = new Base64DecodingStream(os);

            int c;

            while ((c = is.read()) != -1)
            {
                encodingStream.write(c);
            }

            encodingStream.close();
            os.close();

            return os.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
