//
// Created by Andrey Zhmakin, 2017.
//

package zhmakin.base64.tool;

import org.xml.sax.SAXException;
import zhmakin.base64.Base64DecodingStream;
import zhmakin.base64.Base64EncodingStream;
import zhmakin.base64.ExtractBase64Fragments;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;


/**
 * @author Andrey Zhmakin
 */
public class Base64Tool
    extends JFrame
{
    public Base64Tool()
    {
        super("Base64 Tool by Andrey Zhmakin");

        this.initUI();
    }


    private void initUI()
    {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JTabbedPane tabbedPane = new JTabbedPane();

        tabbedPane.addTab("Convert", this.createConvertPane());
        tabbedPane.addTab("Extract", this.createExtractPane());

        Container contentPane = this.getContentPane();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        this.pack();

        this.setSize(400, 300);
        this.setLocationRelativeTo(null);

        this.setVisible(true);
    }


    private Component createConvertPane()
    {
        JPanel root = new JPanel();

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

        root.setLayout(new BorderLayout());
        root.add(splitPane, BorderLayout.CENTER);

        JPanel topPlaceHolder = new JPanel();
        JPanel bottomPlaceHolder = new JPanel();

        topPlaceHolder.setBorder(BorderFactory.createTitledBorder("Plaintext"));
        bottomPlaceHolder.setBorder(BorderFactory.createTitledBorder("Base64"));

        topPlaceHolder.setLayout(new BorderLayout());
        bottomPlaceHolder.setLayout(new BorderLayout());

        JTextArea txaTop = new JTextArea();
        JTextArea txaBottom = new JTextArea();

        topPlaceHolder.add(new JScrollPane(txaTop), BorderLayout.CENTER);
        bottomPlaceHolder.add(new JScrollPane(txaBottom), BorderLayout.CENTER);

        splitPane.setTopComponent   ( topPlaceHolder );
        splitPane.setBottomComponent( bottomPlaceHolder );

        JPanel pnlButtons = new JPanel();
        root.add(pnlButtons, BorderLayout.SOUTH);

        JButton cmdEncode = new JButton("Encode");
        pnlButtons.add(cmdEncode);

        cmdEncode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                String text = txaTop.getText();

                String base64 = Base64EncodingStream.encode(text);
                txaBottom.setText(base64);
            }
        });

        JButton cmdDecode = new JButton("Decode");
        pnlButtons.add(cmdDecode);

        cmdDecode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                String base64 = txaBottom.getText();

                String plaintext = Base64DecodingStream.decode(base64);
                Base64Tool.this.binaryPlaintext = Base64DecodingStream.decodeBytes(base64);

                txaTop.setText(plaintext);
            }
        });

        JButton cmdSavePlaintext = new JButton("Save Plaintext");
        pnlButtons.add(cmdSavePlaintext);

        cmdSavePlaintext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                JFileChooser fc = new JFileChooser();

                String filename;

                if (fc.showSaveDialog(Base64Tool.this) == JFileChooser.APPROVE_OPTION)
                {
                    filename = fc.getSelectedFile().getAbsolutePath();
                }
                else
                {
                    return;
                }

                try
                {
                    File out = new File(filename);
                    FileOutputStream fos = new FileOutputStream(out);
                    fos.write(Base64Tool.this.binaryPlaintext);
                    fos.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

            }
        });

        return root;
    }


    private Component createExtractPane()
    {
        JPanel placeholder = new JPanel();

        placeholder.setLayout(new BorderLayout());

        JPanel pnlFileSelector = new JPanel();
        pnlFileSelector.setLayout(new BorderLayout());

        JPanel pnlEntriesFound = new JPanel();
        pnlEntriesFound.setBorder(BorderFactory.createTitledBorder("Entries found:"));
        placeholder.add(pnlEntriesFound, BorderLayout.CENTER);

        JProgressBar progressBar = new JProgressBar();
        progressBar.setString("");
        progressBar.setStringPainted(true);
        placeholder.add(progressBar, BorderLayout.SOUTH);

        JTable tblEntries
            = new JTable(new DefaultTableModel(new Object[]{"Preview", "Size", "Type"}, 0) )
            {
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
            };

        tblEntries.getTableHeader().setReorderingAllowed(false);

        RowSorter<TableModel> sorter = new TableRowSorter<>(tblEntries.getModel());
        tblEntries.setRowSorter(sorter);

        tblEntries.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        DefaultTableModel model = (DefaultTableModel) tblEntries.getModel();

        tblEntries.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event)
            {
                if (event.getButton() != MouseEvent.BUTTON3)
                {
                    return;
                }

                int rowIndex = tblEntries.rowAtPoint(event.getPoint());

                if (rowIndex >= 0 && rowIndex < tblEntries.getRowCount())
                {
                    if (!tblEntries.isRowSelected(rowIndex))
                    {
                        tblEntries.setRowSelectionInterval(rowIndex, rowIndex);
                    }
                }
                else
                {
                    tblEntries.clearSelection();
                }

                rowIndex = tblEntries.getSelectedRow();

                if (rowIndex < 0)
                {
                    return;
                }

                if (event.isPopupTrigger() && event.getComponent() instanceof JTable)
                {
                    JPopupMenu popup = new JPopupMenu();
                    JMenuItem mitSaveToFile
                        = new JMenuItem(
                            tblEntries.getSelectedRows().length > 1
                                ? "Save to directory..."
                                : "Save to file...");

                    popup.add(mitSaveToFile);
                    mitSaveToFile.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent)
                        {
                            Base64Tool.this.saveToFiles(tblEntries);
                        }
                    });

                    popup.show(event.getComponent(), event.getX(), event.getY());
                }
            }
        });

        JScrollPane scpEntriesScroll = new JScrollPane(tblEntries);
        pnlEntriesFound.setLayout(new BorderLayout());
        pnlEntriesFound.add(scpEntriesScroll, BorderLayout.CENTER);

        JPanel pnlControls = new JPanel();
        pnlControls.setLayout(new GridLayout(0, 1));

        JPanel pnlProperties = new JPanel();
        pnlProperties.setBorder(BorderFactory.createTitledBorder("Properties:"));
        pnlProperties.setLayout(new GridLayout(0, 2));
        pnlProperties.add(new JLabel("Minimal size: "));

        JSpinner spnMinSize = new JSpinner(new SpinnerNumberModel(500, 1, Integer.MAX_VALUE, 1));
        pnlProperties.add(spnMinSize);

        pnlControls.add(pnlFileSelector);
        pnlControls.add(pnlProperties);

        pnlFileSelector.setBorder(BorderFactory.createTitledBorder("Choose file:"));
        placeholder.add(pnlControls, BorderLayout.PAGE_START);

        JTextField txfFilename = new JTextField();
        pnlFileSelector.add(txfFilename, BorderLayout.CENTER);

        JButton cmdChooseFile = new JButton("Browse");
        pnlFileSelector.add(cmdChooseFile, BorderLayout.LINE_END);

        cmdChooseFile.addActionListener(actionEvent -> {
            JFileChooser fc = new JFileChooser();

            if (fc.showOpenDialog(Base64Tool.this) == JFileChooser.APPROVE_OPTION)
            {
                while (model.getRowCount() > 0)
                {
                    model.removeRow(0);
                }

                File inputFile = fc.getSelectedFile();
                String filename = inputFile.getAbsolutePath();

                txfFilename.setText(filename);

                ExtractBase64Fragments fragmentExtractor = new ExtractBase64Fragments();

                final int minSize = Integer.parseInt(spnMinSize.getValue().toString());

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run()
                    {
                        try
                        {
                            progressBar.setIndeterminate(true);
                            progressBar.setString("Extracting fragments...");

                            fragmentExtractor.scrub(inputFile,
                                minSize,
                                bytes -> {
                                    model.addRow(
                                        new Object[]
                                        {
                                            new DataHolder(bytes),
                                            bytes.length,
                                            ExtractBase64Fragments.guessType(bytes)
                                        });
                                });

                            progressBar.setString("Right-click on table items to export");
                        }
                        catch (ParserConfigurationException | SAXException | IOException e)
                        {
                            progressBar.setString("Error while extracting");
                            e.printStackTrace();
                        }
                        finally
                        {
                            progressBar.setIndeterminate(false);
                        }
                    }
                });

                thread.start();
            }
        });

        return placeholder;
    }


    private void saveToFiles(JTable table)
    {
        int[] selectedRows = table.getSelectedRows();
        int entryCount = selectedRows.length;

        JFileChooser fc = new JFileChooser();

        boolean multipleSave = entryCount >= 1;

        if (multipleSave)
        {
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fc.setDialogTitle("Choose a directory to save files in");
        }
        else
        {
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

            // TODO: Suggest file name!
            //DataHolder data = (DataHolder) table.getValueAt(selectedRows[0], 0);
            //fc.setMultiSelectionEnabled(false);
            //fc.setSelectedFile(new File("untitled." + ExtractBase64Fragments.guessType(data.data)));
        }

        if (fc.showSaveDialog(Base64Tool.this) == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                int count = 0;

                for (int row : selectedRows)
                {
                    DataHolder data = (DataHolder) table.getValueAt(row, 0);

                    File file;

                    if (multipleSave)
                    {
                        String dirName = fc.getSelectedFile().getAbsolutePath();

                        if (!dirName.endsWith("/") && !dirName.endsWith("\\"))
                        {
                            dirName = dirName + "/";
                        }

                        file = new File(dirName + count + "." + ExtractBase64Fragments.guessType(data.data));
                    }
                    else
                    {
                        file = fc.getSelectedFile();
                    }

                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(data.data);
                    fos.close();

                    count++;
                }
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }
        }
    }


    public static void main(String[] args)
    {
        new Base64Tool();
    }


    private static class DataHolder
    {
        byte[] data;


        public DataHolder(byte[] data)
        {
            this.data = data;
        }


        @Override
        public String toString()
        {
            byte[] preview = Arrays.copyOfRange(this.data, 0, 16);

            return new String(preview);
        }
    }


    private byte[] binaryPlaintext;
}
