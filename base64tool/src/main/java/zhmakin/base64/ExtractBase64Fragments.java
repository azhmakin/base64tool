//
// Created by Andrey Zhmakin, 2017.
//

package zhmakin.base64;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static zhmakin.base64.ExtractBase64Fragments.BASE64_PATTERN;


/**
 * @author Andrey Zhmakin
 */
public class ExtractBase64Fragments
{
    public interface Callback
    {
        void found(byte[] bytes);
    }


    public final static Pattern BASE64_PATTERN = Pattern.compile("[a-zA-Z\\d+/\\s]+=?\\s?=?");


    public void scrub(File file, int minLength, Callback callback)
        throws ParserConfigurationException, SAXException, IOException
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        Handler handler = new Handler(callback, minLength);
        saxParser.parse(file, handler);
    }


    public static String guessType(byte[] bytes)
    {
        if (bytes.length > 3
            && bytes[0] == (byte) 0xff
            && bytes[1] == (byte) 0xd8
            && bytes[2] == (byte) 0xff)
        {
            return "jpg";
        }

        if (bytes.length > 8
            && bytes[0] == (byte) 0x89
            && bytes[1] == (byte) 0x50
            && bytes[2] == (byte) 0x4e
            && bytes[3] == (byte) 0x47
            && bytes[4] == (byte) 0x0d
            && bytes[5] == (byte) 0x0a
            && bytes[6] == (byte) 0x1a
            && bytes[7] == (byte) 0x0a)
        {
            return "png";
        }

        return "bin";
    }
}


/**
 * @author Andrey Zhmakin
 */
class Handler extends DefaultHandler
{
    ExtractBase64Fragments.Callback callback;
    int minLength;


    public Handler(ExtractBase64Fragments.Callback callback, int minLength)
    {
        this.callback = callback;
        this.minLength = minLength;
    }


    @Override
    public void startElement(String uri,
                             String localName,
                             String qName,
                             Attributes attributes)
        throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String value = attributes.getValue(i);

            this.process(value);
        }
    }


    @Override
    public void endElement(String uri,
                           String localName,
                           String qName)
        throws SAXException
    {

    }


    @Override
    public void characters(char ch[], int start, int length)
        throws SAXException
    {
        String string = new String(ch, start, length);

        this.process(string);
    }


    private void process(String s)
    {
        // TODO: Better minLength cutoff!

        if (s.length() < this.minLength)
        {
            return;
        }

        Matcher m = BASE64_PATTERN.matcher(s);

        while (m.find())
        {
            String match = m.group();

            if (match.length() < this.minLength)
            {
                continue;
            }

            byte[] result = Base64DecodingStream.decodeBytes(match);

            if (result != null && result.length >= this.minLength)
            {
                this.callback.found(result);
            }
        }
    }
}
